import os, os.path as osp
import json
import argparse
from   datetime import datetime
import logging

import praw
from   praw.models    import MoreComments
from   fake_useragent import UserAgent
from   tqdm           import tqdm

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

def get_if_empty(a, b):
    return a if a else b

def get_timestamp_str(format_ = '%Y%m%d_%H%M%S'):
    timestamp = datetime.now().strftime(format_)
    return timestamp

def get_parser():
    ua     = UserAgent()

    parser = argparse.ArgumentParser(
        description = 'Reddit Parser'
    )
    parser.add_argument('--reddit-client-id',
        help     = 'Reddit Client ID',
        required = True
    )
    parser.add_argument('--reddit-client-secret',
        help     = 'Reddit Client Secret',
        required = True
    )
    parser.add_argument('--reddit-username',
        help     = 'Reddit Username',
        required = True
    )
    parser.add_argument('--reddit-password',
        help     = 'Reddit Password',
        required = True
    )
    parser.add_argument('--reddit-user-agent',
        nargs    = '?',
        const    = ua.chrome,
        default  = ua.chrome,
        help     = 'Reddit User Agent'
    )
    parser.add_argument('-s', '--subreddit',
        nargs    = '+',
        help     = 'Subreddit',
        required = True
    )
    parser.add_argument('-t', '--top',
        nargs    = '?',
        const    = 'all',
        default  = 'all',
        help     = 'Top Post Kind'
    )
    parser.add_argument('-p', '--posts',
        type     = int,
        nargs    = '?',
        const    = 25,
        default  = 25,
        help     = 'Number of Posts'
    )
    parser.add_argument('-c', '--comments',
        type     = int,
        nargs    = '?',
        const    = 10,
        default  = 10,
        help     = 'Number of Comments'
    )
    parser.add_argument('--top-half-comments',
        action   = 'store_false',
        default  = False,
        help     = 'Fetch Top Half Comments'
    )
    parser.add_argument('--score-threshold',
        type     = int,
        nargs    = '?',
        const    = -1,
        default  = -1,
        help     = 'Score Threshold'
    )
    parser.add_argument('-o', '--output',
        help     = 'Path to Output File'
    )
    
    return parser

def main():
    parser = get_parser()
    args   = parser.parse_args()

    reddit = praw.Reddit(
        client_id     = args.reddit_client_id,
        client_secret = args.reddit_client_secret,
        username      = args.reddit_username,
        password      = args.reddit_password,
        user_agent    = args.reddit_user_agent
    )

    subreddits  = '+'.join(args.subreddit)
    results     = [ ]

    submissions = list(reddit.subreddit(subreddits).top(args.top, limit = args.posts))

    for submission in tqdm(submissions):
        result = dict()

        log.debug("Parsing Subreddit {subreddit} for Submission {submission}.".format(
            subreddit  = submission.subreddit,
            submission = submission
        ))

        result['subreddit']  = str(submission.subreddit)
        result['submission'] = dict(
            id        = submission.id,
            title     = submission.title,
            url       = submission.url,
            author    = submission.author.name if submission.author else None,
            score     = submission.score,
            created   = str(datetime.fromtimestamp(submission.created))
        )

        comments     = submission.comments
        log.debug("Found {size} Top Level Comments.".format(size = len(comments)))

        if args.top_half_comments:
            comments = comments[:len(comments)/2]
        else:
            comments = comments[:args.comments]

        rcomments    = [ ]
        for comment in comments:
            if not isinstance(comment, MoreComments):
                score = comment.score
                if args.score_threshold == -1 or score >= args.score_threshold:
                    log.debug("Comment (Score: {score}): {comment}".format(
                        score   = comment.score,
                        comment = comment.body
                    ))

                    rcomment    = dict(
                        id      = comment.id,
                        comment = comment.body,
                        author  = comment.author.name if comment.author else None,
                        score   = comment.score,
                        created = str(datetime.fromtimestamp(comment.created))
                    )

                    rcomments.append(rcomment)

        result['comments'] = rcomments

        results.append(result)

    path = osp.abspath(
        get_if_empty(args.output, '{}.json'.format(get_timestamp_str()))
    )

    with open(path, 'w') as f:
        json.dump(results, f, indent = 4)

    print("Saved at {path}".format(path = path))

if __name__ == "__main__":
    main()